#ifndef SIGNALER_H
#define SIGNALER_H

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <signal.h>
#include <sysexits.h>
#include <unistd.h>
#include <limits.h>
#include <fcntl.h>
#include <math.h>

enum {NEG = 45};

// Global variables needed for handling intterupts and command-line
// Arguments
volatile sig_atomic_t got_interupt;
int increment = 1;
unsigned int starting_value = 2;
unsigned int exit_value = UINT_MAX;
bool restart = false;
bool flag_start = false;
bool flag_reverse = false;
bool flag_exit = false;
bool approach_above = false;


bool is_prime(int number);

void signal_handler(int sig);

int command_line_parse(int argc, char *argv[]);

#endif
