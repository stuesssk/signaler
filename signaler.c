#define _XOPEN_SOURCE

#include "signaler.h"


int main(int argc, char *argv[])
{
    // That's a nice PID I got there, I bet it would be easy if
    // know the PID of this process
    pid_t pid;
    pid = getpid();

    // only run if optional command-line flags are present
    if(argc > 1){
        if(command_line_parse(argc, argv)){
            return EX_USAGE;
        }
    }

    // Handles case when start/exit value is primes
    // if start is prime with -s needs to print next prime
    // if exit value is prime, program still needs to print the value
    if (flag_reverse){
        --starting_value;
        --exit_value;
    }else if(flag_start){
        ++starting_value;
        ++exit_value;
    }


    // Code for Setting up signale handler is
    // used from classroom example sigint.c/sigusr.c
    struct sigaction sighandler;

	got_interupt = 0;

	sighandler.sa_handler = signal_handler;
	sighandler.sa_flags = 0;
	sigemptyset(&sighandler.sa_mask);


    // Need to capture SUGHUP/SIGUSR1/SIGUSR2 interupts
	if ((sigaction(SIGUSR1, &sighandler, NULL) == -1) ||
        (sigaction(SIGUSR2, &sighandler, NULL) == -1) ||
        (sigaction(SIGHUP, &sighandler, NULL) == -1)){

		perror("sigaction");
		exit(1);
	}
    for(unsigned int i = starting_value; i !=  exit_value;
                                         i += increment){

        // If the program has to emit a number less than 2
        // it should exit
        if(i < 2){
            // Capturing if reversing reaches the botom of the primes
            if(increment == -1){
                printf("Program has reached the minimum value allowed "
                       "(2).\n");
            // Capturing if the program overflows UINT_MAX
            }else{
                printf("Program has reached the maximum value allowed."
                       "%u\n", UINT_MAX);
            }
            // Program has hit the max or min, time to end it
            break;
        }

        // Starting fresh, got the interupt to start from the beging
        if (restart){
            i = 2;
            restart = false;
        }
        if(is_prime(i)){
            printf("PID%d:%u is Prime\n", pid , i);
            // Need to turn on stdout, since SIGHUP turns it off;[
            freopen("/dev/tty", "a", stdout);
            sleep(1);
        }
    }
    // Found all your primes time to leave
    printf("All primes have been found\n");
    return EX_OK;
}


bool is_prime(int number_testing)
{
    // Need a case to catch 2 or 3, which is prime
    if (number_testing == 2 || number_testing == 3){
        return true;
    // All multiples of 2 or 3 are not prime
    // Eliminates a lot of composite numbers as quickly as possible
    }else if(number_testing % 2 == 0 || number_testing % 3 == 0){
        return false;
    // Only need to check if all the odd number are factor in this loop
    }else{
        // Equation for testing primality sourced from website:
        // https://en.wikipedia.org/wiki/Primality_test
        for(size_t k = 1; (6*k - 1) <= sqrt(number_testing); ++k ){
            // If any number divides evenly, the number in question
            // is not prime
            if(number_testing % (6*k - 1) == 0 ||
               number_testing % (6*k + 1) == 0){
                return false;
            }
        }
        return true;
    }
}

void signal_handler(int sig)
{
    // Catch that interupt
    switch(sig){
        // SIGHUP causes the program to restart emitting primes from 2
	    case SIGHUP:
            printf("Restart From 2!\n");
            increment = 1;
            restart = true;
            // Handle case where exit #  is prime and wee originally 
            // approaching it from above, a restart changes where one 
            // is approaching the exit value, permenently to from below
            if(flag_reverse && approach_above){
                exit_value += 2;
                approach_above = false;            
            }
            break;
        // SIGUSR1 casues the program to skip the next valid prime
        case SIGUSR1:
            printf("SKIPING PRIME!\n");
            // Choose to write to null instead of passing a flag
            // to skip printing a prime
            freopen("/dev/null", "w", stdout);
            break;
        // SIGUSR2 causes the program to reverse the direction
        case SIGUSR2:
            printf("Reversing Direction!\n");
            increment *= -1;
            break;
        default:
            printf("Error: Unexpected value recieved");
    }
}

int command_line_parse(int argc, char *argv[])
{
    // Suppress getopt default error handling
    opterr = 0;

    // Lets get the option flags
    int c;
    while (-1 < (c = getopt(argc, argv, "s:e:rh"))){
        char *errGetOpt;
        switch (c){
            case 's':
                // Determine the starting value of the program
                if(optarg[0] == NEG){
                    printf("Error: Number can not be negative.\n");
                    return 1;
                }
                if(flag_start){
                    printf("Error: Multiple -s options not allowed\n");
                    return 1;
                }
                unsigned long long temp_start =
                                   strtoull(optarg, &errGetOpt, 10);
                // Handle if anything goes wrong
                if (*errGetOpt || errno || temp_start > UINT_MAX ||
                    temp_start < 2){
                    printf("Error: Invalid value for start number\n");
                    printf("Number must be positive number between 2"
                           " and UNIT_MAX.");
                    return 1;
                }
                // Start value has been shown to be good let's store it
                starting_value = temp_start;
                printf("START:%u Looking for next prime.\n",starting_value);
                flag_start = true;
                break;
            case 'r':
                if (flag_reverse){
                    printf("Error: Multiple -r options not allowed\n");
                    return 1;
                }
                increment = -1;
                flag_reverse = true;
                approach_above = true;
                break;
            case 'e':
                // determine the exit number of the program
                if(optarg[0] == NEG){
                    printf("Error: Number can not be negative.\n");
                    return 1;
                }
                if(flag_exit){
                    printf("Error: Multiple -e options not allowed\n");
                    return 1;
                }
                unsigned long long temp_exit =
                                   strtoull(optarg, &errGetOpt, 10);
                if (*errGetOpt || errno || temp_exit > UINT_MAX){
                    printf("Error: Invalid value for exit number\n");
                    return 1;
                }
                // Exit value has been shown to be good let's store it
                exit_value = temp_exit;
                printf("EXIT:%u\n",exit_value);
                flag_exit = true;
                break;
            case 'h':
                // Be nice to the user and explain what the program can
                // do, since a man page is not a flourish
                printf("Signaler options:\n");
                printf("-s <n> starts at the next prime after <n>.\n");
                printf("-r emits in decrease order, must be combined"
                       "with -s.\n");
                printf("-e <n> exit the program if a prime greater "
                       "than <n> would be printed.\n");
                printf("all instances of <n> must be positive numbers");
                return 1;
            default:
                // Invalid input, point user to help
                printf("%s -h for list of options\n",argv[0]);
                return 1;
        }
    }
    return 0;
}
